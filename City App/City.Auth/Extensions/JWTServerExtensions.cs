using System;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using City.Api.Providers;
using City.Infra.Settings;
using City.Application.Features.Cities;
using City.Application.Features.Authentication.Services;

namespace City.Api.Extensions
{
    public static class JWTServerExtensions
    {

        public static void UseJWTServer(this IApplicationBuilder app, IConfiguration configuration)
        {
            var settings = configuration.LoadSettings<AuthSettings>("AuthSettings");

            

            app.UseJwtServer(
                options => {
                    options.TokenEndpointPath = settings.Endpoint;
                    options.AccessTokenExpireTimeSpan = new TimeSpan(settings.Expiration, 0, 0);
                    options.Issuer = settings.Issuer;
                    options.IssuerSigningKey = settings.Secret;
                    options.AuthorizationServerProvider = new CustomAuthorizationServerProvider(app.ApplicationServices.CreateScope().ServiceProvider.GetService<IAuthenticationService>());
                }
            );
        }
        
    }
}

using System.Linq;
using City.Infra.Data.Contexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;

namespace City.Api.Extensions
{
    public static class DbContextExtensions
    {

        public static void ApplyMigrations(this IApplicationBuilder app, IHostingEnvironment environment)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var database = serviceScope.ServiceProvider.GetService<CityDbContext>().Database;
                database.EnsureCreated();
                
                if (!database.IsInMemory() 
                    && !environment.EnvironmentName.Contains("Acceptation") 
                    && database.IsThereMigrationsToApply())
                    database.Migrate();
            }
        }

        public static bool IsThereMigrationsToApply(this DatabaseFacade db)
        {
            var migrationsAssembly = db.GetService<IMigrationsAssembly>();
            var historyRepository = db.GetService<IHistoryRepository>();

            var all = migrationsAssembly.Migrations.Keys;
            var applied = historyRepository.GetAppliedMigrations().Select(r => r.MigrationId);
            var pending = all.Except(applied);

            return pending.Count() > 0;
        }

    }
}

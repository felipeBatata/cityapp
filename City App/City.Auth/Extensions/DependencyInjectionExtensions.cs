using City.Domain.Users;
using City.Infra.Data.Contexts;
using City.Infra.Data.Features.Users;
using City.Infra.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace City.Api.Extensions
{
    public static class DependencyInjectionExtensions
    {
        public static void AddDependencies(this IServiceCollection services, 
            IConfiguration configuration,
            IHostingEnvironment hostingEnvironment)
        {
            var appSettings = configuration.LoadSettings<AppSettings>("AppSettings", services);

            if (hostingEnvironment.EnvironmentName.Contains("Test"))
            {
                services.AddDbContext<CityDbContext>(opt => opt.UseInMemoryDatabase(appSettings.ConnectionString));
            }
            else
            {
                services.AddDbContext<CityDbContext>(opt => opt.UseSqlServer(appSettings.ConnectionString));
            }

            services.AddTransient<IUserRepository, UsersRepository>();
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace City.Api.Extensions
{
    public static class MediatorExtensions
    {

        public static void AddMediator(this IServiceCollection services)
        {
            var assembly = AppDomain.CurrentDomain.Load("City.Application");

            services.AddMediatR(assembly);
        }

    }
}

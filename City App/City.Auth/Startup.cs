using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using City.Api.Extensions;
using City.Application.Features.Cities;
using City.Domain.Users;
using City.Infra.Data.Features.Users;
using City.Application.Features.Cities.Services;
using City.Application.Features.Authentication.Services;

namespace City.Api
{
    public class Startup
    {
        
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }
        
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddDependencies(Configuration, HostingEnvironment);

            services.AddMediator();

            // Ativando o uso de cache em mem�ria
            services.AddMemoryCache();

            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IUserRepository, UsersRepository>();

            services
                .AddJwtServer()
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCORS(Configuration);

            app.UseMvc();

            app.UseJWTServer(Configuration);

            //app.ApplyMigrations(env);
        }
    }
}

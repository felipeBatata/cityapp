using System;
using System.Collections.Concurrent;
using System.Security.Cryptography;
using City.Api.Entities;

namespace City.Api
{
    public class AppClientsStore
    {
        // Store de clients
        public static ConcurrentDictionary<string, AppClient> AppClientsList = new ConcurrentDictionary<string, AppClient>();

        /// <summary>
        /// 
        /// M�todo de inicializa��o que adiciona os client_ids j� gerados na lista
        /// que ser� usada para validar se a chave informada existe ou n�o (no provedor).
        /// 
        /// </summary>
        static AppClientsStore()
        {
            // Adiciona a City.API como um client dessa api de Auth
            AppClientsList.TryAdd("3b9a77fb35a54e40815f4fa8641234c5",
                                   new AppClient
                                   {
                                       ClientId = "3b9a77fb35a54e40815f4fa8641234c5",
                                       Base64Secret = "nbwQ3HDjLNvOnuNyQkBxADEVEwGBEovFZKakYoBQRQo",
                                       Name = "City.API"
                                   });
        }

        /// <summary>
        /// 
        /// Esse m�todo � respons�vel por validar se a API da Aplica��o est� registrada para usar a API de Autentica��o
        /// Tudo isso ocorrer� atrav�s do valor do ClientId informado, que deve ser informado no Request sem a chave criptografada
        /// Diante do "caminho feliz" o contexto do Request ser� marcado como v�lido
        /// 
        /// </summary>
        /// <param name="name">� o nome do novo client. N�o confundir com o client_id, que � o identificador e ser� gerado</param>
        /// <returns>Gera e retorna um client do tipo AppClient</returns>
        public static AppClient AddClient(string name)
        {
            var clientId = Guid.NewGuid().ToString("N");

            var key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            var base64Secret = Convert.ToBase64String(key);

            AppClient newAudience = new AppClient { ClientId = clientId, Base64Secret = base64Secret, Name = name };
            AppClientsList.TryAdd(clientId, newAudience);
            return newAudience;
        }

        /// <summary>
        /// 
        /// Pesquisa na lista de clients cadastrados um client_id informado.
        /// 
        /// </summary>
        /// <param name="clientId">� o identificador client_id que ser� o atributo de busca</param>
        /// <returns>Caso encontre o client_id correspondente, retorna o AppClient. Caso negativo, retorna null.</returns>
        public static AppClient FindClient(string clientId)
        {
            AppClient audience = null;
            if (AppClientsList.TryGetValue(clientId, out audience))
            {
                return audience;
            }
            return null;
        }
    }
}

using City.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace City.Infra.Extensions
{
    public static class EnumerableExtensions
    {
        public static Result<Exception, IQueryable<TSuccess>> AsResult<TSuccess>(this IEnumerable<TSuccess> source)
        {
            return Result.Run(() => source.AsQueryable());
        }
    }
}

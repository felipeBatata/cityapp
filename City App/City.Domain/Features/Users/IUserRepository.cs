using City.Domain.SharedKernel;
using System;
using System.Threading.Tasks;

namespace City.Domain.Users
{
    /// <summary>
    /// Representa o repositório de usuários
    /// </summary>
    public interface IUserRepository
    {
        Task<Result<Exception, User>> GetByCredentials(string email, string password);
    }
}

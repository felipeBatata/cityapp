namespace City.Domain.Users
{
    /// <summary>
    /// Representa um User como entidade de neg�cio
    /// </summary>
    public class User : Entity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}

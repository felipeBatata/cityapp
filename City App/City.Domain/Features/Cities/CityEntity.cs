using City.Domain.Exceptions;
using City.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace City.Domain.Features.Cities
{
    public class CityEntity : Entity
    {
        public string Name { get; set; }

        public int Population { get; set; }

        public DateTime CreationDate { get; set; }

        public string BorderingCities { get; set; }

        [NotMapped]
        public List<int> CitiesId { get; set; }

        public void SetCreationDate()
        {
            CreationDate = DateTime.Now;
        }
    }
}

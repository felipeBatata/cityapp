using City.Domain.SharedKernel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace City.Domain.Features.Cities
{
    /// <summary>
    /// Representa o reposit�rio de Citys
    /// </summary>
    public interface ICityRepository
    {
        Result<Exception, IQueryable<CityEntity>> GetAll();

        /// <summary>
        /// M�todo que retorna uma lista de Citys cadastrados na base de dados juntamente com suas depend�ncias
        /// </summary>
        Result<Exception, IQueryable<CityEntity>> GetAllWithDependencies();

        /// <summary>
        /// Adiciona um novo City na base de dados
        /// </summary>
        // <param name="City">� o City que ser� adicionado da base de dados</param>
        /// <returns>Retorna o novo City com os atributos atualizados (como id)</returns>
        Task<Result<Exception, CityEntity>> AddAsync(CityEntity City);

        /// <summary>
        /// Retorna se um nome de cliente � �nico. 
        /// </summary>
        /// <param name="name">nome do cliente</param>
        /// <param name="id">id a ser ignorado. (util. em caso de update)</param>
        /// <returns>Caso seja �nico retorna true, sen�o retorna false</returns>
        Task<Result<Exception, bool>> IsUniqueAsync(string name, int id = 0);

        /// <summary>
        /// M�todo para atualizar um City na base de dados
        /// </summary>
        Task<Result<Exception, Unit>> UpdateAsync(CityEntity City);

        /// <summary>
        /// M�todo para obter um City espec�fico pelo id sem depend�ncias
        /// </summary>
        /// <param name="CityId">O id do City ue est� sendo pesquisado</param>
        Task<Result<Exception, CityEntity>> GetByIdAsync(int CityId);

        /// <summary>
        /// M�todo para obter um City espec�fico pelo id juntamente com suas depend�ncias
        /// </summary>
        /// <param name="CityId">O id do City ue est� sendo pesquisado</param>
        Task<Result<Exception, CityEntity>> GetByIdWithDependenciesAsync(int CityId);

        /// <summary>
        /// Altera a propriedade isRemoved para true.
        /// </summary>
        // <param name="City">� o City que ser� removido da base de dados</param>
        /// <returns>Retorna um objeto do tipo Result contendo o resultado da opera��o</returns>
        Task<Result<Exception, Unit>> RemoveAsync(CityEntity City);
    }
}

using City.Domain.SharedKernel;

namespace City.Domain
{
    /// <summary>
    /// Todas as classes que representam entidades de neg�cio, devem herdar de Entity.
    /// Entity � uma abstra��o de funcionalidades comuns � entidades de neg�cio.
    ///
    /// Por exemplo: Id, m�todo de compara��o (Equals) e valida��o (Validate)
    ///
    /// </summary>
    public abstract class Entity : IValidationEntity
    {
        /// <summary>
        /// Identificador �nico
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Compara dois objetos com base no id e referencia de mem�ria.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var other = obj as Entity;

            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (Id == 0 || other.Id == 0)
                return false;

            return Id == other.Id;
        }

        /// <summary>
        /// Sobrecarga do operado "==" que compara dois objetos. Nesse caso, ser� comparada a referencia de mem�ria e identificador.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Entity a, Entity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        /// <summary>
        ///     Sobrecarga do operado "!=" que compara dois objetos. Nesse caso, ser� comparada a referencia de mem�ria e identificador.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        /// <summary>
        ///     Sobrecarga do m�todo GetHashCode() para retornar o HashCode do Id
        /// </summary>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Valida a entidade com base em suas regras de neg�cio.
        /// </summary>
        /// <returns></returns>
        public virtual bool Validate()
        {
            return true;
        }
    }
}

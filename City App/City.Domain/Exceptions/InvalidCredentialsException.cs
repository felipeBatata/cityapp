namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio: durante a valida��o das credencias, no login,
    /// as credenciais (email e senha)
    ///
    /// </summary>
    public class InvalidCredentialsException : BusinessException
    {
        public InvalidCredentialsException() : base(ErrorCodes.Unauthorized, "The user name or password is incorrect")
        {
        }
    }
}

namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio: durante uma opera��o,
    /// aos dados informados s�o inv�lidos
    /// </summary>
    public class InvalidObjectException : BusinessException
    {
        public InvalidObjectException() : base(ErrorCodes.InvalidObject, "This object is invalid")
        {
        }
    }
}

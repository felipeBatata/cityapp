using System;

namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio.
    /// � a classe base para a implementa��o de exce��es de neg�cio.
    ///
    /// </summary>
    public class BusinessException : Exception
    {
        public BusinessException(ErrorCodes errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }

        public ErrorCodes ErrorCode { get; }
    }
}

namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio: durante uma opera��o,
    /// o usu�rio n�o tinha permiss�o para realiz�-la.
    /// </summary>
    public class NotAllowedException : BusinessException
    {
        public NotAllowedException() : base(ErrorCodes.NotAllowed, "Operation not allowed")
        {
        }
    }
}

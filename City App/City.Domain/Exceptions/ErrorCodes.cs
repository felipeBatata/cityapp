namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa os c�digos padronizados que ser�o enviador para o cliente
    /// como resposta, no ExceptionPayload.
    ///
    /// </summary>
    public enum ErrorCodes
    {

        /// <summary>
        /// Requisi��o inv�lida: a requisi��o est� com uma sintaxe inv�lida.
        /// </summary>
        BadRequest = 400,

        /// <summary>
        /// Unauthorized: N�o autorizado, a diferen�a para 403 � que o usu�rio n�o est� autenticado.
        /// </summary>
        Unauthorized = 401,

        /// <summary>
        /// A��o proibida. O server entendeu o pedido, mas n�o pode execut�-lo (est� autenticado mas n�o tem permiss�o)
        /// </summary>
        Forbidden = 0403,

        /// <summary>
        ///  N�o encontrado
        /// </summary>
        NotFound = 0404,

        /// <summary>
        /// HttpStatus Conflict: J� existente
        /// </summary>
        AlreadyExists = 0409,

        /// <summary>
        /// equivale ao httpStatus 405 not allowed
        /// </summary>
        NotAllowed = 0405,

        /// <summary>
        /// Objeto invalido equivale ao httpStaus 422 Unprocessable Entity
        /// </summary>
        InvalidObject = 0422,

        /// <summary>
        /// Exce��o n�o tratada.(Internal Server error)
        /// </summary>
        Unhandled = 0500,

        /// <summary>
        /// Servi�o n�o dispon�vel
        /// </summary>
        ServiceUnavailable = 0503,
    }
}

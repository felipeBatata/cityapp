namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio: durante o cadastro,
    /// a entidade j� foi cadastrada com os dados informados
    ///
    /// </summary>
    public class AlreadyExistsException : BusinessException
    {
        public AlreadyExistsException() : base(ErrorCodes.AlreadyExists, "This registry already exists")
        {
        }
    }
}

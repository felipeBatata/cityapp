namespace City.Domain.Exceptions
{
    /// <summary>
    /// Representa uma exce��o de neg�cio: o recurso solicitado n�o pode ser encontrado
    /// </summary>
    public class NotFoundException : BusinessException
    {
        public NotFoundException() : base(ErrorCodes.NotFound, "Registry not found")
        {
        }
    }
}

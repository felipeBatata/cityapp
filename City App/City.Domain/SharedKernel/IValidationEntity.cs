namespace City.Domain.SharedKernel
{
    /// <summary>
    /// Representa a valida��o de uma entidade de neg�cio
    /// </summary>
    public interface IValidationEntity
    {
        bool Validate();
    }
}

﻿using City.Domain.Features.Cities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace City.Domain.Service
{
    public class CityPathService
    {
        public List<int> _visitedCities = new List<int>();
        public Dictionary<int, List<int>> _borderingCities = new Dictionary<int, List<int>>();
        public bool _isFinish;

        public string Get(int from, int to, List<CityEntity> cities)
        {
            var cityFrom = cities.FirstOrDefault(x => x.Id == from);
            var cityTo = cities.FirstOrDefault(x => x.Id == to);

            _isFinish = false;

            _visitedCities.Add(cityFrom.Id);
            _borderingCities = PopulateBorderingCities(cities);
            GetPath(cityFrom.Id, cityTo.Id);

            if (!_visitedCities.Any())
                return string.Empty;
            else
            {
                var builder = new StringBuilder();

                foreach (var visitedCityId in _visitedCities)
                {
                    var cittVisited = cities.FirstOrDefault(x => x.Id == visitedCityId);

                    builder.Append(cittVisited.Name).Append(",");
                }

                return RemoveLastChar(builder.ToString());
            }
        }

        private static String RemoveLastChar(String str)
        {
            return str.Substring(0, str.Length - 1);
        }

        private static Dictionary<int, List<int>> PopulateBorderingCities(List<CityEntity> cities)
        {
            var IdWithBorderingCities = new Dictionary<int, List<int>>();

            foreach (var item in cities)
                IdWithBorderingCities.Add(item.Id, item.CitiesId);

            return IdWithBorderingCities;
        }

        private void GetPath(int cityFrom, int cityTo)
        {
            foreach (var borderCity in _borderingCities[cityFrom])
            {
                if (!_visitedCities.Contains(borderCity))
                {
                    if (!_isFinish)
                        _visitedCities.Add(borderCity);

                    if (borderCity == cityTo)
                    {
                        _isFinish = true;
                        break;
                    }

                    if (!_isFinish)
                    {
                        GetPath(borderCity, cityTo);

                        if (!_visitedCities.Contains(cityTo))
                            _visitedCities.Remove(borderCity);
                    }
                }
            }
        }
    }
}

using Microsoft.EntityFrameworkCore;
using City.Domain.Users;
using City.Infra.Data.Features.Cities;
using City.Domain.Features.Cities;
using System.Collections.Generic;
using System;

namespace City.Infra.Data.Contexts
{
    public class CityDbContext : DbContext
    {
        public CityDbContext(DbContextOptions<CityDbContext> options) : base(options)
        {
            //Password = teste, criptografado = h8Xv+yFL9ttSGgHn/HSc1EcwrxMfWFGj0Ql5siFDBE4=
            Users.AddAsync(new User { Name = "Knewin", Password = "h8Xv+yFL9ttSGgHn/HSc1EcwrxMfWFGj0Ql5siFDBE4=" });

            PopulateCity();
        }

        public DbSet<CityEntity> Cities { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CityEntityConfiguration());

            modelBuilder.City();

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
            .UseLazyLoadingProxies(false);

        public void PopulateCity()
        {
            var city = new CityEntity { Id = 1, Name = "Lages", Population = 170000, CreationDate = DateTime.Now, BorderingCities = "Otacilio Costa, Sao Joaquim", CitiesId = new List<int> { 6, 2 } };
            var city2 = new CityEntity { Id = 2, Name = "S�o Joaquim", Population = 26000, CreationDate = DateTime.Now, BorderingCities = "Lages, Bom Jardim", CitiesId = new List<int> { 1, 3 } };
            var city3 = new CityEntity { Id = 3, Name = "Bom Jardim", Population = 9000, CreationDate = DateTime.Now, BorderingCities = "Sao Joaquim, Lauro Muller", CitiesId = new List<int> { 2, 4 } };
            var city4 = new CityEntity { Id = 4, Name = "Lauro Muller", Population = 15000, CreationDate = DateTime.Now, BorderingCities = "Bom Jardim, Orleans", CitiesId = new List<int> { 3, 5 } };
            var city5 = new CityEntity { Id = 5, Name = "Orleans", Population = 20000, CreationDate = DateTime.Now, BorderingCities = "Lauro Muller", CitiesId = new List<int> { 4 } };
            var city6 = new CityEntity { Id = 6, Name = "Otacilio Costa", Population = 18000, CreationDate = DateTime.Now, BorderingCities = "Lages", CitiesId = new List<int> { 1 } };
            Cities.AddAsync(city);
            Cities.AddAsync(city2);
            Cities.AddAsync(city3);
            Cities.AddAsync(city4);
            Cities.AddAsync(city5);
            Cities.AddAsync(city6);
        }
    }
}

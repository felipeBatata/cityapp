using Microsoft.EntityFrameworkCore;
using City.Domain.Users;
using City.Infra.Crypto;

namespace City.Infra.Data.Contexts
{
    public static class CityExtensions
    {
        /// <summary>
        /// M�todo City utilizado para criar o usu�rio padr�o para a aplica��o.
        /// ALERTA: ESTE M�TODO DEVE SER UTILIZADO SOMENTE PARA O PROP�SITO DESCRITO ACIMA. 
        /// N�O UTILIZ�-LO PARA POPULAR O BANCO COM DADOS N�O NECESS�RIOS PARA A INICIALIZA��O DA APLICA��O
        /// </summary>
        /// <param name="builder"></param>
        public static void City(this ModelBuilder builder)
        {
            var password = "321";

            builder.Entity<User>().HasData(new User() { Id = 1, Email = "admin@admin.com", Name = "Admin", Password = password.GenerateHash() });
        }

    }
}

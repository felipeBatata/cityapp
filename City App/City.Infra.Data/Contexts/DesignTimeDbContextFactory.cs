using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace City.Infra.Data.Contexts
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CityDbContext>
    {
        public CityDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CityDbContext>();
            optionsBuilder.UseSqlServer("Server=localHost;Database=CityDBContext;Trusted_Connection=True;MultipleActiveResultSets=true",
                opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return new CityDbContext(optionsBuilder.Options);
        }
    }
}

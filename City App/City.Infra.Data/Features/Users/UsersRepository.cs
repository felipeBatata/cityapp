using Microsoft.EntityFrameworkCore;
using City.Domain.Exceptions;
using City.Domain.SharedKernel;
using City.Domain.Users;
using City.Infra.Data.Contexts;
using System;
using System.Threading.Tasks;

namespace City.Infra.Data.Features.Users
{
    /// <summary>
    ///  Repositório de usuários
    /// </summary>
    public class UsersRepository : IUserRepository
    {
        private readonly CityDbContext _context;

        public UsersRepository(CityDbContext context)
        {
            _context = context;
        }

        public async Task<Result<Exception, User>> GetByCredentials(string email, string password)
        {
            //var user = await _context.Users.FirstOrDefaultAsync(u => u.Email.Equals(email) && u.Password == password);
            //if (user == null)
            //{
            //    return new InvalidCredentialsException();
            //}

            //return user;

           return new User { Name = "bc76c0f3-ee37-41b4-a58f-acb27fafc33e", Password = "Teste"};

        }
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using City.Domain.Features.Cities;

namespace City.Infra.Data.Features.Cities
{
    /// <summary>
    /// Configuração do banco para a entidade City
    /// </summary>
    public class CityEntityConfiguration : IEntityTypeConfiguration<CityEntity>
    {
        public void Configure(EntityTypeBuilder<CityEntity> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50);
            builder.Property(c => c.BorderingCities).IsRequired();
            builder.Property(c => c.CreationDate).IsRequired();
        }
    }
}

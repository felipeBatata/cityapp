using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using City.Domain.Features.Cities;
using City.Infra.Data.Contexts;
using City.Domain.SharedKernel;
using City.Infra.Extensions;
using City.Domain.Exceptions;
using System.Collections.Generic;

namespace City.Infra.Data.Features.Cities
{
    /// <summary>
    /// Reposit�rio de Cities
    /// </summary>
    public class CityEntityRepository : ICityRepository
    {
        private CityDbContext _context;
        private List<CityEntity> _cities;

        public CityEntityRepository(CityDbContext context)
        {
            _context = context;
            _cities = new List<CityEntity>();
            PopulateCity();
        }

        /// <summary>
        /// Adiciona um novo CityEntity na base de dados
        /// </summary>
        // <param name="CityEntity">� o CityEntity que ser� adicionado da base de dados</param>
        /// <returns>Retorna o novo CityEntity com os atributos atualizados (como id)</returns>
        public async Task<Result<Exception, CityEntity>> AddAsync(CityEntity CityEntity)
        {
            CityEntity.Id = _cities.Last().Id + 1;
            _cities.Add(CityEntity);
            //await _context.SaveChangesAsync();
            return CityEntity;
        }

        /// <summary>
        /// Retorna se um nome de cliente � �nico. 
        /// </summary>
        /// <param name="name">nome do cliente</param>
        /// <param name="id">id a ser ignorado. (util. em caso de update)</param>
        /// <returns>Caso seja �nico retorna true, sen�o retorna false</returns>
        public async Task<Result<Exception, bool>> IsUniqueAsync(string name, int id = 0)
        {
            return !( _cities.Any(c => c.Name.ToLower().Equals(name.ToLower()) && (id == 0 || c.Id != id)));
        }

        /// <summary>
        /// M�todo que retorna uma lista de Cities cadastrados na base de dados sem depend�ncias
        /// </summary>
        public  Result<Exception, IQueryable<CityEntity>> GetAll()
        {
            return  _cities.AsResult();
        }

        /// <summary>
        /// M�todo que retorna uma lista de Cities cadastrados na base de dados juntamente com suas depend�ncias
        /// </summary>
        public Result<Exception, IQueryable<CityEntity>> GetAllWithDependencies()
        {
            return _cities
                        .AsResult();
        }

        /// <summary>
        /// M�todo para obter um CityEntity espec�fico pelo id sem suas depend�ncias
        /// </summary>
        /// <param name="CityEntityId">O id do CityEntity que est� sendo pesquisado</param>
        public async Task<Result<Exception, CityEntity>> GetByIdAsync(int CityEntityId)
        {
            var CityEntity =  _cities.FirstOrDefault(c => c.Id == CityEntityId);

            if (CityEntity == null)
                return new NotFoundException();
            return CityEntity;
        }

        /// <summary>
        /// M�todo para obter um CityEntity espec�fico pelo id juntamente com suas depend�ncias
        /// </summary>
        /// <param name="CityEntityId">O id do CityEntity ue est� sendo pesquisado</param>
        public async Task<Result<Exception, CityEntity>> GetByIdWithDependenciesAsync(int CityEntityId)
        {
            var CityEntity =  _cities
                                .FirstOrDefault(c => c.Id == CityEntityId);

            if (CityEntity == null)
                return new NotFoundException();
            return CityEntity;
        }

        /// <summary>
        /// M�todo para atualizar um CityEntity na base de dados
        /// </summary>
        public async Task<Result<Exception, Unit>> UpdateAsync(CityEntity CityEntity)
        {
            _context.Cities.Update(CityEntity);

            await _context.SaveChangesAsync();
            return Unit.Successful;
        }

        /// <summary>
        /// Altera a propriedade isRemoved para true.
        /// </summary>
        // <param name="CityEntity">� o CityEntity que ser� removido da base de dados</param>
        /// <returns>Retorna um objeto do tipo Result contendo o resultado da opera��o</returns>
        public async Task<Result<Exception, Unit>> RemoveAsync(CityEntity CityEntity)
        {
            _cities.Remove(CityEntity);
            //_context.Entry(CityEntity).State = EntityState.Deleted;
            //await _context.SaveChangesAsync();
            return Unit.Successful;
        }

        public void PopulateCity()
        {
            var city = new CityEntity { Id = 1, Name = "Lages", Population = 170000, CreationDate = DateTime.Now, BorderingCities = "Otacilio Costa, Sao Joaquim", CitiesId = new List<int> { 6, 2 } };
            var city2 = new CityEntity { Id = 2, Name = "S�o Joaquim", Population = 26000, CreationDate = DateTime.Now, BorderingCities = "Lages, Bom Jardim", CitiesId = new List<int> { 1, 3 } };
            var city3 = new CityEntity { Id = 3, Name = "Bom Jardim", Population = 9000, CreationDate = DateTime.Now, BorderingCities = "Sao Joaquim, Lauro Muller", CitiesId = new List<int> { 2, 4 } };
            var city4 = new CityEntity { Id = 4, Name = "Lauro Muller", Population = 15000, CreationDate = DateTime.Now, BorderingCities = "Bom Jardim, Orleans", CitiesId = new List<int> { 3, 5 } };
            var city5 = new CityEntity { Id = 5, Name = "Orleans", Population = 20000, CreationDate = DateTime.Now, BorderingCities = "Lauro Muller", CitiesId = new List<int> { 4 } };
            var city6 = new CityEntity { Id = 6, Name = "Otacilio Costa", Population = 18000, CreationDate = DateTime.Now, BorderingCities = "Lages", CitiesId = new List<int> { 1 } };
            _cities.Add(city);
            _cities.Add(city2);
            _cities.Add(city3);
            _cities.Add(city4);
            _cities.Add(city5);
            _cities.Add(city6);
        }
    }
}

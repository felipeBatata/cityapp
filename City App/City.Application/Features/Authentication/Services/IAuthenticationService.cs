﻿using City.Domain.SharedKernel;
using City.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace City.Application.Features.Authentication.Services
{
    public interface IAuthenticationService
    {
        Task<Result<Exception, User>> GetUser(string userName, string password);
    }
}

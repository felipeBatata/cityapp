﻿using City.Domain.SharedKernel;
using City.Domain.Users;
using City.Infra.Crypto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace City.Application.Features.Authentication.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private IUserRepository _userRepository;

        public AuthenticationService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Result<Exception, User>> GetUser(string userName, string password)
        {
            return await _userRepository.GetByCredentials(userName, password.GenerateHash());
        }
    }
}

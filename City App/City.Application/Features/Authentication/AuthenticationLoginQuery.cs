using City.Domain.SharedKernel;
using City.Domain.Users;
using FluentValidation;
using MediatR;
using System;

namespace City.Application.Features.Authentication
{
    public class AuthenticationLoginQuery : IRequest<Result<Exception, User>>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class AuthenticationLoginQueryValidator : AbstractValidator<AuthenticationLoginQuery>
    {
        public AuthenticationLoginQueryValidator()
        {
            RuleFor(d => d.UserName).NotEmpty().WithMessage("Nome de usu�rio n�o informado");

            RuleFor(d => d.Password).NotEmpty().WithMessage("Senha n�o informada");
        }
    }
}

using City.Domain.SharedKernel;
using City.Domain.Users;
using City.Infra.Crypto;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Authentication
{
    public class AuthenticationLoginHandler : IRequestHandler<AuthenticationLoginQuery, Result<Exception, User>>
    {
        private IUserRepository _userRepository;

        private IMemoryCache _cache;

        public AuthenticationLoginHandler(IUserRepository userRepository, IMemoryCache memoryCache)
        {
            _userRepository = userRepository;
            _cache = memoryCache;
        }

        public async Task<Result<Exception, User>> Handle(AuthenticationLoginQuery request, CancellationToken cancellationToken)
        {
            Result<Exception, User> cacheEntry;

            if (!_cache.TryGetValue(CacheKeys.Entry, out cacheEntry))
            {
                var password = request.Password.GenerateHash();
                cacheEntry = await _userRepository.GetByCredentials(request.UserName, password);

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(30));

                _cache.Set(CacheKeys.Entry, cacheEntry, cacheEntryOptions);
            }

            return cacheEntry;
        }
    }
}

using MediatR;
using City.Application.Features.Cities.Queries;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Handlers
{
    public class CityFindHandler : IRequestHandler<CityGetQuery, Result<Exception, CityEntity>>
    {
        private readonly ICityRepository _repositoryCity;

        public CityFindHandler(ICityRepository CityRepository)
        {
            _repositoryCity = CityRepository;
        }

        public async Task<Result<Exception, CityEntity>> Handle(CityGetQuery request, CancellationToken cancellationToken)
        {
            return await _repositoryCity.GetByIdWithDependenciesAsync(request.CityId);
        }
    }
}

using MediatR;
using City.Application.Features.Cities.Commands;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Handlers
{
    public class CityDeleteHandler : IRequestHandler<CityRemoveCommand, Result<Exception, Domain.SharedKernel.Unit>>
    {
        private readonly ICityRepository _repositoryCity;

        public CityDeleteHandler(ICityRepository CityRepository)
        {
            _repositoryCity = CityRepository;
        }

        public async Task<Result<Exception, Domain.SharedKernel.Unit>> Handle(CityRemoveCommand request, CancellationToken cancellationToken)
        {
            foreach (var CityId in request.CityIds)
            {
                var CityCB = await _repositoryCity.GetByIdAsync(CityId);

                if (CityCB.IsFailure)
                    return CityCB.Failure;

                var result = await _repositoryCity.RemoveAsync(CityCB.Success);
                if (result.IsFailure)
                    return result.Failure;
            }

            return Domain.SharedKernel.Unit.Successful;
        }
    }
}

using MediatR;
using City.Application.Features.Cities.Queries;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Handlers
{
    public class CityLoadAllHandler : IRequestHandler<CityLoadAllQuery, Result<Exception, IQueryable<CityEntity>>>
    {
        private readonly ICityRepository _repositoryCity;

        public CityLoadAllHandler(ICityRepository CityRepository)
        {
            _repositoryCity = CityRepository;
        }

        public Task<Result<Exception, IQueryable<CityEntity>>> Handle(CityLoadAllQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_repositoryCity.GetAll());
        }
    }
}

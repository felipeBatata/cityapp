using AutoMapper;
using MediatR;
using City.Application.Features.Cities.Commands;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Handlers
{
    public class CityUpdateHandler : IRequestHandler<CityUpdateCommand, Result<Exception, Domain.SharedKernel.Unit>>
    {
        private readonly ICityRepository _repositoryCity;

        public CityUpdateHandler(ICityRepository CityRepository)
        {
            _repositoryCity = CityRepository;
        }

        public async Task<Result<Exception, Domain.SharedKernel.Unit>> Handle(CityUpdateCommand request, CancellationToken cancellationToken)
        {
            var CityCB = await _repositoryCity.GetByIdAsync(request.Id);

            if (CityCB.IsFailure)
                return CityCB.Failure;

            var City = CityCB.Success;
            Mapper.Map(request, City);

            return await _repositoryCity.UpdateAsync(City);
        }
    }
}

using AutoMapper;
using MediatR;
using City.Application.Features.Cities.Commands;
using City.Domain.Exceptions;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Handlers
{
    public class CityCreateHandler : IRequestHandler<CityRegisterCommand, Result<Exception, int>>
    {
        private readonly ICityRepository _repositoryCity;

        public CityCreateHandler(ICityRepository CityRepository)
        {
            _repositoryCity = CityRepository;
        }
    
        public async Task<Result<Exception, int>> Handle(CityRegisterCommand request, CancellationToken cancellationToken)
        {
            var City = Mapper.Map<CityRegisterCommand, CityEntity>(request);

            City.SetCreationDate();

            var isUniqueCallback = await _repositoryCity.IsUniqueAsync(City.Name);

            if (isUniqueCallback.IsFailure)
                return isUniqueCallback.Failure;
            if (!isUniqueCallback.Success)
                return new AlreadyExistsException();

            var addCityCallback = await _repositoryCity.AddAsync(City);

            if (addCityCallback.IsFailure)
                return addCityCallback.Failure;

            var newCity = addCityCallback.Success;

            return newCity.Id;
        }
    }
}

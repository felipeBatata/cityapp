using System;
using City.Domain.SharedKernel;
using FluentValidation;
using MediatR;

namespace City.Application.Features.Cities.Commands
{
    public class CityRemoveCommand : IRequest<Result<Exception, Domain.SharedKernel.Unit>>
    {
        public virtual int[] CityIds { get; set; }
    }

    public class CityRemoveCommandValidator : AbstractValidator<CityRemoveCommand>
    {
        public CityRemoveCommandValidator()
        {
            RuleFor(c => c.CityIds).NotEmpty();
        }
    }
}

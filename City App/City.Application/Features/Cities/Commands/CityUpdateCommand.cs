using FluentValidation;
using MediatR;
using System;
using City.Domain.SharedKernel;

namespace City.Application.Features.Cities.Commands
{
    public class CityUpdateCommand : IRequest<Result<Exception, Domain.SharedKernel.Unit>>
    {
        public virtual int Id { get; set; }
        public string Name { get; set; }
        public int Population { get; set; }
    }

    public class CityUpdateCommandValidator : AbstractValidator<CityUpdateCommand>
    {
        public CityUpdateCommandValidator()
        {
            RuleFor(c => c.Name).NotNull().NotEmpty();
            RuleFor(c => c.Population).NotNull().NotEmpty();
        }
    }
}

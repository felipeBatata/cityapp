using FluentValidation;
using MediatR;
using System;
using City.Domain.SharedKernel;

namespace City.Application.Features.Cities.Commands
{
    public class CityRegisterCommand : IRequest<Result<Exception, int>>
    {
        public string Name { get; set; }
        public int Population { get; set; }
        public string BorderingCities { get; set; }
    }
    
    public class CityRegisterCommandValidator : AbstractValidator<CityRegisterCommand>
    {
        public CityRegisterCommandValidator()
        {
            RuleFor(c => c.Name).NotNull().NotEmpty().Matches(@"^[a-zA-Z0-9\-_]{1,50}$");
            RuleFor(c => c.Population).NotNull().NotEmpty();
        }
    }
}

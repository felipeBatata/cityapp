﻿using AutoMapper;
using City.Application.Features.Cities.Commands;
using City.Application.Features.Cities.Queries;
using City.Domain.Exceptions;
using City.Domain.Features.Cities;
using City.Domain.Service;
using City.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Services
{
    public class CityService : ICityService
    {
        private ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public async Task<Result<Exception, int>> CreateCity(CityRegisterCommand request)
        {
            var City = Mapper.Map<CityRegisterCommand, CityEntity>(request);

            City.SetCreationDate();

            var isUniqueCallback = await _cityRepository.IsUniqueAsync(City.Name);

            if (!isUniqueCallback.Success)
                throw new AlreadyExistsException();

            var user = await _cityRepository.AddAsync(City);

            return user.Success.Id;
        }

        public async Task<Result<Exception, Unit>> DeleteCity(CityRemoveCommand request )
        {
            foreach (var CityId in request.CityIds)
            {
                var CityCB = await _cityRepository.GetByIdAsync(CityId);

                if (CityCB.IsFailure)
                    return CityCB.Failure;

                var result = await _cityRepository.RemoveAsync(CityCB.Success);
                if (result.IsFailure)
                    return result.Failure;
            }

            return Unit.Successful;
        }

        public async Task<Result<Exception, CityEntity>> GetCityById(CityGetQuery request)
        {
            return await _cityRepository.GetByIdWithDependenciesAsync(request.CityId);
        }

        public Task<Result<Exception, IQueryable<CityEntity>>> GetAll()
        {
            return Task.FromResult(_cityRepository.GetAll());
        }

        public async Task<Result<Exception, Unit>> Update(CityUpdateCommand request)
        {
            var CityCB = await _cityRepository.GetByIdAsync(request.Id);

            if (CityCB.IsFailure)
                return CityCB.Failure;

            var City = CityCB.Success;
            Mapper.Map(request, City);

            return await _cityRepository.UpdateAsync(City);
        }

        public async Task<Result<Exception, string>> GetBordering(CityGetBorderingQuery request)
        {
            var CityCB = await _cityRepository.GetByIdAsync(request.CityId);

            if (CityCB.IsFailure)
                return CityCB.Failure;

            var City = CityCB.Success;

            return City.BorderingCities;
        }

        public async Task<Result<Exception, int>> SumPopulation(CitySumPopulationQuery request)
        {
            var CityCB =  _cityRepository.GetAll();

            if (CityCB.IsFailure)
                return CityCB.Failure;

            var cities = CityCB.Success;

            return cities.Where(x => request.CityIds.Contains(x.Id))
                .Select(x => x.Population)
                .Sum();
        }

        public async Task<Result<Exception, string>> GetPathBetweenCities(CityBetweenQuery request)
        {
            if (request.CityFrom == request.CityTo)
                return $"Cidade de origem e de destino são iguais.";

            var CityCB = _cityRepository.GetAll();

            if (CityCB.IsFailure)
                return CityCB.Failure;

            var cities = CityCB.Success.ToList();

            return new CityPathService().Get(request.CityFrom, request.CityTo, cities);
        }
    }
}

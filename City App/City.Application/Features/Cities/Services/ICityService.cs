﻿using City.Application.Features.Cities.Commands;
using City.Application.Features.Cities.Queries;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City.Application.Features.Cities.Services
{
    public interface ICityService
    {
        Task<Result<Exception, int>> CreateCity(CityRegisterCommand request);

        Task<Result<Exception, Unit>> DeleteCity(CityRemoveCommand request);

        Task<Result<Exception, CityEntity>> GetCityById(CityGetQuery request);

        Task<Result<Exception, IQueryable<CityEntity>>> GetAll();

        Task<Result<Exception, Unit>> Update(CityUpdateCommand request);

        Task<Result<Exception, string>> GetBordering(CityGetBorderingQuery request);

        Task<Result<Exception, int>> SumPopulation(CitySumPopulationQuery request);

        Task<Result<Exception, string>> GetPathBetweenCities(CityBetweenQuery request);
    }
}

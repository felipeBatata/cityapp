using FluentValidation;
using MediatR;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;

namespace City.Application.Features.Cities.Queries
{
    public class CityGetQuery : IRequest<Result<Exception, CityEntity>>
    {

        public virtual int CityId { get; set; }

    }

    public class CityGetQueryValidator : AbstractValidator<CityGetQuery>
    {
        public CityGetQueryValidator()
        {
            RuleFor(d => d.CityId).GreaterThan(0);
        }
    }
}

﻿using City.Domain.SharedKernel;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace City.Application.Features.Cities.Queries
{
    public class CityBetweenQuery : IRequest<Result<Exception, string>>
    {
        public int CityFrom { get; set; }

        public int CityTo { get; set; }
    }

    public class CityBetweenQueryValidator : AbstractValidator<CityBetweenQuery>
    {
        public CityBetweenQueryValidator()
        {
            RuleFor(d => d.CityFrom).NotEmpty();
            RuleFor(d => d.CityTo).NotEmpty();
        }
    }
}

using MediatR;
using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using System;
using System.Linq;

namespace City.Application.Features.Cities.Queries
{
    public class CityLoadAllQuery : IRequest<Result<Exception, IQueryable<CityEntity>>>
    {
    }
}

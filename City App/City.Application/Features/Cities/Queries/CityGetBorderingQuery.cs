﻿using City.Domain.Features.Cities;
using City.Domain.SharedKernel;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace City.Application.Features.Cities.Queries
{
    public class CityGetBorderingQuery : IRequest<Result<Exception, CityEntity>>
    {
        public int CityId { get; set; }

        public string Name { get; set; }
    }

    public class CityGetBorderingValidator : AbstractValidator<CityGetBorderingQuery>
    {
        public CityGetBorderingValidator()
        {
            RuleFor(d => d.CityId).GreaterThan(0);
            RuleFor(d => d.Name).NotEmpty();
        }
    }
}

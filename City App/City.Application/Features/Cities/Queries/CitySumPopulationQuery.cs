﻿using City.Domain.SharedKernel;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace City.Application.Features.Cities.Queries
{
    public class CitySumPopulationQuery : IRequest<Result<Exception, int>>
    {
        public List<int> CityIds { get; set; }

        public CitySumPopulationQuery()
        {
            CityIds = new List<int>();
        }
    }

    public class CitySumPopulationValidator : AbstractValidator<CitySumPopulationQuery>
    {
        public CitySumPopulationValidator()
        {
            RuleFor(c => c.CityIds).NotEmpty();
        }
    }
}

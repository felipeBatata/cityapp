using AutoMapper;
using City.Application.Features.Cities.Commands;
using City.Domain.Features.Cities;

namespace City.Application.Features.Cities
{
    /// <summary>
    ///
    ///  Realiza o mapeamento entre o Command/Query e a entidade de dom�nio City
    ///
    /// </summary>
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CityRegisterCommand, CityEntity>();
            CreateMap<CityUpdateCommand, CityEntity>();
        }
    }
}

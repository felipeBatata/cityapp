using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using City.API.Exceptions;
using City.Domain.Exceptions;

namespace City.API.Filters
{
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// M�todo invocado quando ocorre uma exce��o no controller
        /// </summary>
        /// <param name="context">� o contexto atual da requisi��o</param>
        public override void OnException(ExceptionContext context)
        {
            context.Exception = context.Exception;
            context.HttpContext.Response.StatusCode = ErrorCodes.Unhandled.GetHashCode();
            context.Result = new JsonResult(ExceptionPayload.New(context.Exception, ErrorCodes.Unhandled.GetHashCode()));
        }
    }
}

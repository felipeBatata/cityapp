using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace City.API.Filters
{
    /// <summary>
    /// Atributo de uso [ODataQueryOptionsValidate] para validar as op��es de odata nos controllers (ODataQueryOptions).
    ///
    /// Nesse atributo � configurado o que pode ou n�o ser executado no odata.
    /// Por padr�o, desativa a op��o de $expand.
    ///
    /// </summary>
    public class ODataQueryOptionsValidateAttribute : ActionFilterAttribute
    {
        private ODataValidationSettings oDataValidationSettings;

        /// <summary>
        ///  No construtor, cria uma nova configura��o de valida��o para odata
        /// </summary>
        public ODataQueryOptionsValidateAttribute(AllowedQueryOptions allowedQueryOptions = AllowedQueryOptions.All ^ AllowedQueryOptions.Expand)
        {
            oDataValidationSettings = new ODataValidationSettings() { AllowedQueryOptions = allowedQueryOptions };
        }

        /// <summary>
        /// Ao receber uma chamada http no controller, � invocado esse m�todo para validar as op��es do odata
        /// </summary>
        ///
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.ActionArguments.Any(a => a.Value != null && a.Value.GetType().Name.Contains(typeof(ODataQueryOptions).Name)))
            {
                var odataQueryOptions = (ODataQueryOptions)actionContext.ActionArguments.Where(a => a.Value != null && a.Value.GetType().Name.Contains(typeof(ODataQueryOptions).Name)).FirstOrDefault().Value;
                odataQueryOptions.Validate(oDataValidationSettings);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}

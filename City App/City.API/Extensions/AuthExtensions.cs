using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using City.API.Handlers;
using City.Infra.Settings;

namespace City.API.Extensions
{
    public static class AuthExtensions
    {

        public static void AddAuth(this IServiceCollection services, IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            if (hostingEnvironment.EnvironmentName.Contains("ByPassingAuth"))
            {
                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = "Test Scheme"; // Tem que combinar com o TestAuthenticationExtensions
                    options.DefaultChallengeScheme = "Test Scheme";
                })
                .AddTestAuth(o => { });
            }
            else
            {
                var authSettings = configuration.LoadSettings<AuthSettings>("AuthSettings", services);

                services
                    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(jwtBearerOptions =>
                    {
                        jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateActor = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = authSettings.Issuer,
                            ValidAudience = authSettings.ClientId,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authSettings.Secret))
                        };
                    });
            }
        }

    }
}

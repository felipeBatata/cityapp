using Microsoft.Extensions.DependencyInjection;
using City.Infra.Extensions;

namespace City.API.Extensions
{
    public static class AutoMapperExtensions
    {

        public static void AddAutoMapper(this IServiceCollection services)
        {
            services.ConfigureProfiles(typeof(API.Startup), typeof(Application.AppModule));
        }

    }
}

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using City.Infra.Settings;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;
using City.Infra.Data.Contexts;
using City.Domain.Features.Cities;
using City.Infra.Data.Features.Cities;

namespace City.API.Extensions
{
    public static class SimpleInjectorExtensions
    {
        public static void AddSimpleInjector(this IServiceCollection services, Container container)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            services.AddSingleton<IControllerActivator>(new SimpleInjectorControllerActivator(container));
            services.UseSimpleInjectorAspNetRequestScoping(container);

            services.EnableSimpleInjectorCrossWiring(container);
        }

        public static void AddDependencies(this IServiceCollection services,
            Container container,
            IConfiguration configuration,
            IHostingEnvironment hostingEnvironment)
        {
            if (hostingEnvironment.EnvironmentName.Contains("Test"))
            {
                container.Register(() =>
                {
                    var options = new DbContextOptionsBuilder<CityDbContext>().UseInMemoryDatabase("DBTest").Options;
                    return new CityDbContext(options);
                }, Lifestyle.Scoped);
            }
            else
            {
                var appSettings = configuration.LoadSettings<AppSettings>("AppSettings", services);

                container.Register(() =>
                {
                    var options = new DbContextOptionsBuilder<CityDbContext>().UseSqlServer(appSettings.ConnectionString).Options;
                    return new CityDbContext(options);
                }, Lifestyle.Scoped);
            }

            //� necess�rio registrar para utilizar o i18n controller. Com o DI resolver padr�o do asp.net core n�o � necess�rio fazer isso
            //container.RegisterInstance<IHostingEnvironment>(hostingEnvironment);

            container.Register<ICityRepository, CityEntityRepository>();
        }
    }
}

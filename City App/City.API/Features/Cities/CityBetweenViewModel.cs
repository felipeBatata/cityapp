﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace City.API.Features.Cities
{
    public class CityBetweenViewModel
    {
        public int FromCity { get; set; }

        public int ToCity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace City.API.Features.Cities
{
    public class CityBorderingViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}

using MediatR;
using Microsoft.AspNetCore.Mvc;
using City.API.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using City.Application.Features.Cities.Commands;
using City.Application.Features.Cities.Queries;
using City.Application.Features.Cities.Services;
using City.Domain.Features.Cities;
using AutoMapper;
using System.Linq;

namespace City.API.Features.Cities
{
    [Authorize]
    [Route("api/[controller]")]
    public class CitiesController : ControllerBase
    {
        private readonly ICityService _cityService;

        public CitiesController(ICityService cityService) : base()
        {
            _cityService = cityService;
        }

        #region HttpGet

        [ProducesResponseType(typeof(CityViewModel), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [Route("{id:int}")]
        [HttpGet]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var query = new CityGetQuery() { CityId = id };

            var result = await _cityService.GetCityById(query);

            return Ok(result); 
        }

        [ProducesResponseType(typeof(List<CityViewModel>), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var query = await _cityService.GetAll();

            var listViewModel = new List<CityViewModel>();

            foreach (var city in query.Success.ToList())
            {
                var mapperCity = Mapper.Map<CityEntity, CityViewModel> (city);

                listViewModel.Add(mapperCity);
            }

            return Ok(listViewModel);
        }

        #endregion HttpGet

        #region HttpPost

        [HttpPost]
        [ProducesResponseType(typeof(CityViewModel), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        public async Task<IActionResult> PostAsync([FromBody] CityRegisterCommand CityCmd)
        {
           var city = await _cityService.CreateCity(CityCmd);

            if (city.IsFailure)
                return BadRequest();

            return Ok(city.Success);
        }

        #endregion HttpPost

        #region HttpPut

        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [HttpPut]
        [Route("{CityId:int}")]
        public async Task<IActionResult> PutAsync(int CityId, [FromBody] CityUpdateCommand command)
        {
           var result =  await _cityService.Update(command);

            if (result.IsFailure)
                return BadRequest();

            return Ok(result.Success);
        }

        #endregion HttpPut

        #region HttpDelete

        [ProducesResponseType(typeof(CityViewModel), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync([FromBody] CityRemoveCommand command)
        {
            var result = await _cityService.DeleteCity(command);

            if (result.IsFailure)
                return BadRequest();

            return Ok(result.Success);
        }

        #endregion HttpDelete

        /// <summary>
        /// Retorna as cidades que fazem fronteira com determinada cidade
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [HttpPost]
        [Route("cityBordering")]
        public async Task<IActionResult> BorderingCity([FromBody] CityBorderingViewModel command)
        {
            var query = new CityGetBorderingQuery() { CityId = command.Id, Name = command.Name };

            var result = await _cityService.GetBordering(query);

            return Ok(result.Success);
        }

        /// <summary>
        /// Retorna a soma da popula��o das cidades informadas 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [HttpPost]
        [Route("sumPopulation")]
        public async Task<IActionResult> SumPopulation([FromBody] CitySumPopulationViewModel command)
        {
            var query = new CitySumPopulationQuery () { CityIds = command.CityIds };

            var result = await _cityService.SumPopulation(query);

            return Ok(result.Success);
        }

        /// <summary>
        /// Retorna o melhor caminho entre duas cidades
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ExceptionPayload), 400)]
        [HttpPost]
        [Route("betweenCities")]
        public async Task<IActionResult> BetweenCities([FromBody] CityBetweenViewModel command)
        {
            var query = new CityBetweenQuery() { CityFrom = command.FromCity, CityTo = command.ToCity };

            var result = await _cityService.GetPathBetweenCities(query);

            return Ok(result.Success);
        }
    }
}

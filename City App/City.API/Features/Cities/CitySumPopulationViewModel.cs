﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace City.API.Features.Cities
{
    public class CitySumPopulationViewModel
    {
        public List<int> CityIds { get; set; }
    }
}

using AutoMapper;
using City.API.Features.Cities;
using City.Domain.Features.Cities;

namespace City.Application.Features.Cities
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CityEntity, CityDetailViewModel>();
            CreateMap<CityEntity, CityViewModel>();
        }
    }
}

namespace City.API.Features.Cities
{
    public class CityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Population { get; set; }

        public string BorderingCities { get; set; }
    }
}

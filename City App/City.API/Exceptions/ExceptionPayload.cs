using System;
using System.Collections.Generic;

namespace City.API.Exceptions
{
    /// <summary>
    ///  Classe que representa uma exce��o lan�ada para o client como resposta.
    ///</summary>
    public class ExceptionPayload
    {
        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public List<ValidationFailure> Errors { get; set; }

        /// <summary>
        /// M�todo para criar um novo ExceptionPayload de uma exce��o de neg�cio.
        ///          
        /// As exce��es de neg�cio, que s�o providas no City.Domain
        /// s�o identificadas pelos c�digos no enum ErrorCodes. 
        /// 
        /// Assim, esse m�todo monta o ExceptionPayload, que ser� o c�digo retornado o cliente, 
        /// com base na exce��o lan�ada.
        /// 
        /// </summary>
        /// <param name="exception">� a exce��o lan�ada</param>
        /// <param name="errorCode">C�digo HTTP de erro</param>
        /// <param name="failures">Lista de problemas de valida��o</param>
        /// <returns>ExceptionPayload contendo o c�digo do erro e a mensagem da da exce��o que foi lan�ada </returns>
        public static ExceptionPayload New<T>(T exception, int errorCode, List<ValidationFailure> failures = null) where T : Exception
        {
            return new ExceptionPayload
            {
                ErrorCode = errorCode,
                ErrorMessage = exception.Message,
                Errors = failures,
            };
        }
    }
}

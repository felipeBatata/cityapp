using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using FluentValidation.Results;

namespace City.API.Exceptions
{
    public class ValidationFailureMapper
    {

        public List<ValidationFailure> Map(IEnumerable<FluentValidation.Results.ValidationFailure> failures)
        {
            return failures.AsQueryable().ProjectTo<ValidationFailure>().ToList();
        }

    }
}

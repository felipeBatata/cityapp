﻿using System;

namespace City.Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Escolha uma Opção:");
            Console.WriteLine("1) Duplicados na lista");
            Console.WriteLine("2) Palindromo");
            Console.WriteLine("3) Sair");
            Console.Write("\r\nSelecione uma opção: ");

            switch (Console.ReadLine())
            {
                case "1":
                    GetRepeatIndex();
                    return true;
                case "2":
                    IsPalindrome();
                    return true;
                case "3":
                    return false;
                default:
                    return true;
            }
        }

        public static void GetRepeatIndex()
        {
            int[] array1 = { 55, 48, 91, 5, 781, 8, 10, 25, 23, 2, 4, 88, 45, 147, 545, 781, 523 };
            bool find = false;

            Console.WriteLine("Lista a ser verificada: 55,48, 91, 5,781, 8, 10, 25, 23, 2, 4, 88,45, 147,545,781,523");

            for (int i = 0; i < array1.Length; i++)
            {
                for (int j = i + 1; j < array1.Length; j++)
                {
                    var pos1 = array1[i];
                    var pos2 = array1[j];

                    if (pos1 == pos2)
                    {
                        Console.WriteLine($"Posição do primeiro índice repetido : {i}, valor: {pos1} se repetindo no índice {j}");
                        find = true;
                        Console.ReadKey();
                    }
                }
            }

            if (find == false)
            {
                Console.WriteLine($"Não há indice repetidos");
                Console.ReadKey();
            }
        }

        public static void IsPalindrome()
        {
            Console.WriteLine("Digite a palavra para ser verificado se a mesma é um palindromo ou não:");

            var myString = Console.ReadLine();

            string first = myString.Substring(0, myString.Length / 2);
            char[] arr = myString.ToCharArray();

            Array.Reverse(arr);

            string temp = new string(arr);
            string second = temp.Substring(0, temp.Length / 2);

            var result = first.Equals(second);

            if (result)
                Console.WriteLine($"{myString} é um palindromo!!!");
            else
                Console.WriteLine($"{myString} não é um palindromo!!!");

            Console.ReadKey();
        }
    }
}

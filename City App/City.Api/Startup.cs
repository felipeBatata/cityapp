using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using City.API.Extensions;
using SimpleInjector;
using City.Application.Features.Cities.Services;
using City.Domain.Features.Cities;
using City.Infra.Data.Features.Cities;
using City.Infra.Data.Contexts;
using Microsoft.EntityFrameworkCore;

namespace City.API
{
    public class Startup
    {

        public static Container Container = new Container();
        
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }
        
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();
            services.AddOData();
            services.AddCors();
            services.AddSimpleInjector(Container);
            services.AddDependencies(Container, Configuration, HostingEnvironment);
            services.AddAuth(Configuration, HostingEnvironment);
            services.AddValidators(Container);
            services.AddFilters();
            services.AddSwagger();
            services.AddMVC();

            services.AddDbContext<CityDbContext>(opt =>
            {
                opt.UseSqlServer(Configuration.GetConnectionString("cityDBContext"),
                    b => b.MigrationsAssembly("City.API"));
            }).AddEntityFrameworkInMemoryDatabase();

            services.AddTransient<ICityService, CityService>();
            services.AddTransient<ICityRepository, CityEntityRepository>();


            services.BuildServiceProvider();
        }

        // Este m�todo � chamado em tempo de execu��o. Use este m�todo para configurar suas solicita��es
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            Container.AutoCrossWireAspNetComponents(app);

            // Habilitanto o uso de autentica��o
            app.UseAuthentication();

            app.UseCORS(Configuration);

            Container.RegisterMvcControllers(app);
            
            app.ConfigSwagger();
            
            app.UseOData();

            //app.ApplyMigrations(Container, env);
        }
    }
}

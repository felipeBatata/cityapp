using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;

namespace City.Infra.Crypto
{
    /// <summary>
    /// Classe para manipular a cripografia no servidor
    /// </summary>
    public static class CryptoExtensions
    {
        /// <summary>
        /// M�todo para gerar uma hash com base no algoritmo HMACSHA1
        /// </summary>
        /// <param name="passwordToHash">� a string atual (m�todo de extens�o) que ser� a base para a gera��o da hash</param>
        /// <returns>Retorna a hash do valor da string atual</returns>
        public static string GenerateHash(this string passwordToHash)
        {
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: passwordToHash,
                salt: new byte[0],
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}

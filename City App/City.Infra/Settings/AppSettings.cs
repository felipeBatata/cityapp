namespace City.Infra.Settings
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string Secret { get; set; }
    }
}

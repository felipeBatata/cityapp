using System;
using System.Collections.Generic;
using System.Text;

namespace City.Infra.Settings
{
    /// <summary>
    /// Representa��o das configura��es dos CORS da aplica��o
    /// </summary>
    public class CORSSettings
    {
        /// <summary>
        /// Clientes que tem permiss�o para fazer requisi��es a API.
        /// Ex: "http://localhost:8081", ... ou "*" para permitir todas as origens
        /// </summary>
        public string[] Origins { get; set; }

        /// <summary>
        /// M�todos HTTP que a API tem permiss�o para disponibilizar o acesso
        /// Ex: "GET", "POST", "PUT", "DELETE",... ou "*" para permitir todos os m�todos
        /// </summary>
        public string[] Methods { get; set; }

        /// <summary>
        /// Cabe�alhos HTTP que a API est� autorizada a aceitar
        /// Ex: "Authorization", "Accept", ... ou "*" para permitir todos os cabe�alhos
        /// </summary>
        public string[] Headers { get; set; }

        public CORSSettings()
        {
        }

        /// <summary>
        /// Cria uma configura��o padr�o quando n�o � informado no appsettings.json nenhuma configura��o para o CORS (se��o CORSSettings)
        /// </summary>
        /// <returns>Configura��o padr�o (permitindo tudo)</returns>
        public CORSSettings Default()
        {
            Origins = new string[] { "*" };
            Methods = new string[] { "*" };
            Headers = new string[] { "*" };

            return this;
        }
    }
}
